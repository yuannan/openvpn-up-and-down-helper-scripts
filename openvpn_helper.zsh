#!/usr/bin/env zsh

case $1 in
0)
	echo "No kill switch enabled"
	;;
1)
	killswitch_system_lan_up.sh && echo "Kill switch with LAN done."
	;;
2)
	killswitch_system_nolan_up.sh && echo "Kill switch with NO LAN done."
	;;
3)
	dns_leaks.zsh
esac

# plug dns leak or hooks
if [[ $2 = 1 ]]; then
	echo "Running systemctl hooks!"
	if [[ $script_type == "up" ]]; then
		systemctl restart dnscrypt-proxy.service && echo "Restarted dnscrypt-proxy service" && echo "up" > /etc/openvpn/status
	fi
fi

# start display manager
if [[ $3 = 1 ]]; then
	if [[ $(systemctl is-active sddm) != "active" ]]; then
		systemctl start sddm
	fi
fi
