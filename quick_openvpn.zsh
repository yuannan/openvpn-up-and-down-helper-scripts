#/usr/bin/zsh
#debug=true
if [[ $1 == "up" ]]; then
	echo "Going up!"

	enc="ed448"
	profile=$2
	: ${profile:="Balancer_UDP.ovpn"}
	# kill
	# 0 = none
	# 1 = lan
	# 2 = no lan
	# 3 = dns leaks only
	kills=1
	hooks=1
	dm=1

	# OpenVPN helper
	# this is done as the scripts can only run one script
	# the helper therefore lets me more easily hook in different scripts
	opvh=$(which openvpn_helper.zsh)

	# debug output or run
	cmd="openvpn --script-security 2 --config /etc/openvpn/$enc/$profile --up \"$opvh $kills $hooks $dm\" --down \"$opvh $kills $hooks $dm\" &"
	if [[ -v debug ]]; then
		echo $cmd
	else
		eval $cmd
	fi

elif [[ $1 == "down" ]]; then
	echo "Going down!"
	if [[ -e /etc/openvpn/status ]]; then
		rm /etc/openvpn/status && echo "Cleaned up CryptoStorm status"
	fi
else
	echo "arg 1 needs to be up or down"
fi
