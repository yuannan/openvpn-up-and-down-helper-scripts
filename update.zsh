#!/usr/bin/env zsh
#debug=true
#verbose=true

# config
basedir=$PWD
pass="/etc/openvpn/cspass"

# debug and verbose prints
d() {
	if [ $debug ]; then
		echo $1
	fi
}

v() {
	if [ $verbose ]; then
		echo $1
	fi
}

# init types
types=("rsa" "ecc" "ed25519" "ed448")

# init locations
ahead="https://cryptostorm.is/configs/"
taddres=("rsa" "ecc" "ecc/ed25519" "ecc/ed448")
atail="/configs.zip"

# cleanup
for t in $types; do
	if [ -d $basedir/$t ]; then
		rm -rf $t && v "Removed $t"
	else
		v "The dir does not exist $t"
	fi
done
v "Cleanup done\n"

# new configs time B)
umask 277
for ((t = 0; t < ${#types[@]}; t++)); do
	type=$types[$t+1]

	# make new dir
	d "\nMaking new type: $type"
	newconfigdir=$basedir/$type
	mkdir $newconfigdir && v "Made $newconfigdir"

	# change into the dir
	cd $newconfigdir && d $PWD
	
	# time to download the new configs
	dladr=$ahead$taddres[$t+1]$atail
	d $dladr
	if [ $verbose ]; then
		d "verbose mode"
		wget $dladr && v "Downloaded $dladr"
		unzip "configs.zip" && v "Unziped $type configs"
	else
		d "!verbose mode"
		wget $dladr 2> /dev/null && v "Downloaded $dladr"
		unzip "configs.zip" > /dev/null && v "Unziped $type configs"
	fi

	# add pass token
	scmd="sed -e's_^auth-user-pass.*_auth-user-pass $pass"
	scmd+="_' -i *.ovpn"
	d $scmd
	eval $scmd

	# set perms
	chmod 500 *.ovpn && v "Permissions set"

	# go back to base dir for the next loop
	cd $basedir && d $PWD && v ""
done

v "Done."
