This repo is made for Arch Linux and systemd distros. Systemd is only used to run quick_openvpn.zsh
which should setup a secure OpenVPN tunnel along with Firewalling and IPv6 Blocking.

Install:

/etc/openvpn:
	- update.zsh (from this repo)
	- cshash (cryptostorm hashed token)
	- cspass (cryptostorm hashed token followed by anything)

update.zsh will now pull the new configs from cryptostorm and insert the cspass into the configs.
NO args are taken at runtime. Debug and verbose mode can be enabled manually by uncommenting the global var.

/usr/local/bin (or any other common PATH location):
	- quick_openvpn.zsh (quickly runs openvpn with default options)
	- openvpn_helper.zsh (used to consolidate the --up and --down for OpenVPN scripts)
